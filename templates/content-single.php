<?php while (have_posts()) : the_post(); ?>
  <?php $image_id = get_field('title_background');
  $image_array = wp_get_attachment_image_src( $image_id, 'title-image');

  if ($image_id === NULL) {
    $image = get_template_directory_uri() . '/dist/images/title-1.jpg';
  } else {
    $image = $image_array[0];
  }

  ?>
  <article <?php post_class(); ?>>
    <header class="page-header" style="background-image: url('<?php echo $image; ?>');">
      <div class="container">
        <h1 class="entry-title"><?php the_title(); ?></h1>
          <?php get_template_part('templates/entry-meta'); ?>
        ?>
      </div>
    </header>
    <div class="entry-content container">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
<?php $image_dir = get_template_directory_uri(); ?>

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php wp_head(); ?>
  <link href='https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,700|Open+Sans:300,400,700,400italic' rel='stylesheet' type='text/css'>
  <link rel="apple-touch-icon" sizes="57x57" href="<?= $image_dir ?>/fg/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?= $image_dir ?>/favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?= $image_dir ?>/favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?= $image_dir ?>/favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?= $image_dir ?>/favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?= $image_dir ?>/favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?= $image_dir ?>/favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?= $image_dir ?>/favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?= $image_dir ?>/favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="<?= $image_dir ?>/favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?= $image_dir ?>/favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?= $image_dir ?>/favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?= $image_dir ?>/favicon/favicon-16x16.png">
  <link rel="manifest" href="<?= $image_dir ?>/favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#000">
  <meta name="msapplication-TileImage" content="<?= $image_dir ?>/favicon/ms-icon-144x144.png">
  <meta name="theme-color" content="#009B39">
</head>

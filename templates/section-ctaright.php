<?php $image_id = get_sub_field('cta_image');
$image = wp_get_attachment_image_src( $image_id, 'half-image' ); ?>

<section class="right-cta">
	<div class="wrapper">
		<div class="content">
			<?php the_sub_field('editor_content'); ?>
		</div>
		<div class="image" style="background-image: url('<?php echo $image[0] ?>');">
			<h3><?php the_sub_field('cta_heading'); ?></h3>
			<a href="<?php the_sub_field('cta_link'); ?>" class="btn btn-primary"><?php the_sub_field('cta_button_text'); ?></a>
		</div>
	</div>
</section>
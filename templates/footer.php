<footer class="content-info">
	<div class="footer-1">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
          <?php // WP_Query arguments
          $args = array (
            'post_type'              => array( 'definition' ),
            'posts_per_page'         => '1',
            'orderby'                => 'rand',
          );

          // The Query
          $definition = new WP_Query( $args );

          if ( $definition->have_posts() && !is_front_page() ) {
            while ( $definition->have_posts() ) {
              $definition->the_post(); ?>
                <span class="word"><?php the_title(); ?></span>
                <?php the_content(); ?>
            <?php }
          } else { ?>
            <span class="word">nu·cle·us</span>
            <p>ˈn(y)o͞oklēəs/</p>
            <p>noun: nucleus; plural noun: nuclei</p>
            <p>1- the central and most important part of an object, movement, or group, forming the basis for its activity and growth.<br>2- Synonyms: core, center, central part, heart, nub, hub, middle, eye, focus, focal point, pivot, crux</p>
          <?php }

          // Restore original Post Data
          wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
	</div>
  <div class="container footer-2">
    <div class="row">
    	<div class="col-sm-4">
    		<img src="<?= get_template_directory_uri(); ?>/dist/images/logo-horizontal.png">
    		<p>PO Box 5887<br>Carefree, AZ 85377<br>(602) 721 1488</p>
    	</div>
    	<div class="col-sm-2">
  			<?php
  			if (has_nav_menu('footer_1_navigation')) :
  			  wp_nav_menu(['theme_location' => 'footer_1_navigation', 'menu_class' => 'nav']);
  			endif;
  			?>
    	</div>
    	<div class="col-sm-2">
    		<?php
  			if (has_nav_menu('footer_2_navigation')) :
  			  wp_nav_menu(['theme_location' => 'footer_2_navigation', 'menu_class' => 'nav']);
  			endif;
  			?>
    	</div>
    	<div class="col-sm-4">
    		<h3>Connect With Us</h3>
    		<ul class="list-unstyled list-inline social-menu">
    			<li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
    			<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
    			<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
    			<li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i></a></li>
    		</ul>
        <ul class="list-unstyled list-inline site-icons">
          <li><i class="fa fa-recycle" aria-hidden="true"></i></li>
          <li><i class="fa fa-cc-visa" aria-hidden="true"></i></li>
          <li><i class="fa fa-cc-mastercard" aria-hidden="true"></i></li>
        </ul>
  		</div>
    </div>
    <p class="copyrights">© <?php echo date("Y") ?> Nucleus Construction. All Rights Reserved.</p>
    <p class="credit">Created By <a href="http://stephengreer.me/">Stephen Greer</a></p>
  </div>
</footer>

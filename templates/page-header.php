<?php use Roots\Sage\Titles; ?>

<?php $image_id = get_field('title_background');
$image_array = wp_get_attachment_image_src( $image_id, 'title-image');

if ($image_id != NULL ) {
	$image = $image_array[0];
} else {
	$image = get_template_directory_uri() . '/dist/images/title-1.jpg';
}

?>
<div class="page-header" style="background-image: url('<?php echo $image; ?>');">
	<div class="overlay"></div>
	<div class="container">
	  <h1><?= Titles\title(); ?></h1>
  </div>
</div>
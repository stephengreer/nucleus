<?php $image_id = get_sub_field('divider_background');
$image = wp_get_attachment_image_src( $image_id, 'title-image' ); ?>

<section class="divider" style="background-image: url('<?php echo $image[0] ?>');">
	<div class="container">
		<h2><?php the_sub_field('divider_heading'); ?></h2>
		<a href="<?php the_sub_field('button_link'); ?>" class="btn btn-primary"><?php the_sub_field('button_text'); ?></a>
	</div>
</section>
<section class="small-gallery">
	<div class="container">
		<div class="intro">
			<h2><?php esc_html( the_sub_field('section_title') ); ?></h2>
			<?php esc_html( the_sub_field('section_description') ); ?>
		</div>
	</div>
	<div class="gallery-slider">
			<?php
			// check if the repeater field has rows of data
			if( have_rows('images') ):

			 	// loop through the rows of data
			    while ( have_rows('images') ) : the_row(); ?>

			  		<?php $image_id = get_sub_field('image');
			  		$image = wp_get_attachment_image( $image_id, 'tile-image' ); 
			  		$image_url = wp_get_attachment_image_src( $image_id, 'large' ); ?>

			  		<a href="<?= $image_url[0] ?>" class="swipebox"><?php echo $image ?></a>

			    <?php endwhile;
			else :
		    echo "no slides found";
			endif;
			?>
	</div>
</section>
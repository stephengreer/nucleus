<?php 

$login_page = get_permalink( 132 );

if ( is_user_logged_in() ) {
  $client_page = get_permalink( 144 );
  $contractor_page = get_permalink( 413 );
  $user_ID = get_current_user_id();
  $user_info = get_userdata($user_ID);
  $user_role_1 = $user_info->roles;
  $user_role = implode( $user_role_1 );
}

if (!is_user_logged_in() && is_page_template( 'template-client.php' )) {
  header("Location: $login_page");
  exit;
} elseif (!is_user_logged_in() && is_page_template( 'template-contractor.php' )) {
  header("Location: $login_page");
  exit;
} elseif (is_user_logged_in() && $user_role === 'contractor' && is_page_template( 'template-client.php' )) {
  header("Location: $contractor_page");
  exit;
} elseif (is_user_logged_in() && $user_role === 'client' && is_page_template( 'template-contractor.php' )) {
  header("Location: $client_page");
  exit;
}

if (!is_user_logged_in() && is_page_template( 'template-private.php' )) {
  header("Location: $login_page");
  exit;
}

?>
<section class="half-carousel">
	<div class="container">
		<div class="intro">
			<h2><?php esc_html( the_sub_field('section_title') ); ?></h2>
			<?php esc_html( the_sub_field('section_description') ); ?>
		</div>
	</div>

	<div class="half-slides">
		<?php
		// check if the repeater field has rows of data
		if( have_rows('slides') ):

		 	// loop through the rows of data
		    while ( have_rows('slides') ) : the_row(); ?>

		  		<?php $image_id = get_sub_field('image');
		  		$image = wp_get_attachment_image( $image_id, 'half-image' ); ?>

		  		<div class="slide">
		  		  <div class="content">
		  				<?php the_sub_field('content'); ?>
		  			</div>
		  			<div class="image">
		  				<?php echo $image ?>
		  			</div>
		  		</div>

		    <?php endwhile;
		else :
	    echo "no slides found";
		endif;
		?>
	</div>
</section>
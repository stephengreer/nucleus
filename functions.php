<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php', // Theme customizer
  'lib/wp_bootstrap_navwalker.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);

add_image_size( 'hero-image', 1920, 1080, array( 'center', 'center' ) );
add_image_size( 'title-image', 1920, 600, array( 'center', 'center' ) );
add_image_size( 'half-image', 720, 460, array( 'center', 'center' ) );
add_image_size( 'tile-image', 640, 640, array( 'center', 'center' ) );


add_action('wp_print_styles', 'my_deregister_styles', 100);

function my_deregister_styles() {
  wp_deregister_style('wpum-frontend-css');
}

// Remove Menu Items
function remove_menus() {
  if ( !current_user_can( 'edit_themes' ) ) {
    remove_menu_page( 'edit-comments.php' );          //Comments
    remove_menu_page( 'themes.php' );                 //Appearance
    remove_menu_page( 'plugins.php' );                //Plugins
    remove_menu_page( 'tools.php' );                  //Tools
    remove_menu_page( 'options-general.php' );        //Settings
    remove_menu_page( 'edit.php?post_type=acf-field-group' );        //Custom Fields
    remove_menu_page( 'users.php?page=wpum-profile-fields' );        //Custom Fields
  }
}
add_action( 'admin_init', 'remove_menus', 1 );

// Remove Submenu Items
function remove_submenus() {
  if ( !current_user_can( 'edit_themes' ) ) {
    remove_submenu_page( 'users.php', 'wpum-profile-fields' );
    remove_submenu_page( 'edit.php', 'wpum_directory' );
    remove_submenu_page( 'users.php', 'wpum-settings' );
    remove_submenu_page( 'users.php', 'wpum-tools' );
    remove_submenu_page( 'plugin-install.php', 'wpum-addons' );
  }
}
add_action( 'admin_init', 'remove_submenus', 1 );

// Add Menu Items
function add_menu_items() {
  if ( !current_user_can( 'edit_themes' ) ) {
    add_menu_page( 'menu', 'Menu', 'edit_pages', 'nav-menus.php', '', ( '' ), 25 );
  }
}
add_action( 'admin_menu', 'add_menu_items' );

// Rename Posts to Blog Posts in Menu
function rename_menu_item() {
  if ( !current_user_can( 'edit_themes' ) ) {
      global $menu;
      $menu[5][0] = 'Blog Posts';
  }
}
add_action( 'admin_menu', 'rename_menu_item' );

function redirect_backend()
{
  if(!current_user_can('edit_posts'))
  {
    wp_redirect(get_permalink( 144 ));
    exit();
  }
}
add_action('admin_init', 'redirect_backend');
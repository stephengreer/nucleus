<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php $intro_p = get_field('intro_paragraph'); 
  if ($intro_p != NULL) { ?>
  <div class="page-intro">
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
  				<?php echo $intro_p ?>
  			</div>
  		</div>
  	</div>
	</div>
	<?php } ?>
	<div class="page-content">
	  <div class="container">
	  	<div class="row">
	  		<div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
				  <?php get_template_part('templates/content', 'page'); ?>
			  </div>
		  </div>
	  </div>
  </div>
<?php endwhile; ?>
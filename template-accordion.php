<?php
/**
 * Template Name: Accordion Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php $intro_p = get_field('intro_paragraph'); 
  if ($intro_p != NULL) { ?>
  <div class="page-intro">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
          <?php echo $intro_p ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
        
          <?php if( have_rows('accordion') ): ?>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <?php $i=1; while ( have_rows('accordion') ) : the_row(); ?>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading-<?php echo $i; ?>">
                      <h2 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse-<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
                         <?php the_sub_field('item_title'); ?>
                        </a>
                      </h2>
                    </div>
                    <div id="collapse-<?php echo $i; ?>" class="panel-collapse collapse <?php if ($i==1) { echo 'in'; } ?>" role="tabpanel" aria-labelledby="heading-<?php echo $i; ?>">
                      <div class="panel-body">
                        <?php the_sub_field('item_content'); ?>
                      </div>
                    </div>
                </div>
              <?php $i++; endwhile; ?>
            </div>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
<?php endwhile; ?>
<?php
/**
 * Template Name: Work Template
 */
?>

<?php get_template_part('templates/page', 'header'); ?>

<div class="work-title container">
  <h2>Featured Houses</h2>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi feugiat a turpis non rutrum. In ac enim lacus. Integer pretium est nisi, eu auctor diam accumsan at.</p>
</div>

<div class="work-gallery">
  <?php $args = array (
    'post_type'              => array( 'house' ),
  );

  $rooms = new WP_Query( $args );

  if ( $rooms->have_posts() ) {
    while ( $rooms->have_posts() ) {
      $rooms->the_post(); ?>

      <?php $image_id = get_field('featured_image');
      $image = wp_get_attachment_image( $image_id, 'tile-image' );
      ?>
      <div class="work-gallery-item">
        <a href="<?php the_permalink(); ?>"><span class="link-spanner"></span></a>
        <?php echo $image ?>
        <div class="item-content-wrapper">
          <div class="item-content">
            <h3><?php the_title(); ?></h3>
              <?php $intro_p = get_field('intro_paragraph'); 
              if ($intro_p != NULL) { ?>
                  <?php echo $intro_p ?>
              <?php } ?>
          </div>
        </div>
      </div>
    <?php }
  } else {
    echo '<h3>No Posts Found</h3>';
  }

  wp_reset_postdata(); ?>
</div>

<div class="work-title container">
  <h2>Featured Rooms</h2>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi feugiat a turpis non rutrum. In ac enim lacus. Integer pretium est nisi, eu auctor diam accumsan at.</p>
</div>

<div class="work-gallery">
  <?php $args = array (
    'post_type'              => array( 'room' ),
  );

  $rooms = new WP_Query( $args );

  if ( $rooms->have_posts() ) {
    while ( $rooms->have_posts() ) {
      $rooms->the_post(); ?>

      <?php $image_id = get_field('featured_image');
      $image = wp_get_attachment_image( $image_id, 'tile-image' );
      ?>
      <div class="work-gallery-item">
        <a href="<?php the_permalink(); ?>"><span class="link-spanner"></span></a>
        <?php echo $image ?>
        <div class="item-content-wrapper">
          <div class="item-content">
            <h3><?php the_title(); ?></h3>
              <?php $intro_p = get_field('intro_paragraph'); 
              if ($intro_p != NULL) { ?>
                  <?php echo $intro_p ?>
              <?php } ?>
          </div>
        </div>
      </div>
    <?php }
  } else {
    echo '<h3>No Posts Found</h3>';
  }

  wp_reset_postdata(); ?>
</div>
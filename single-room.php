<?php while (have_posts()) : the_post(); ?>
  
  <?php $image_id = get_field('featured_image');
  $image = wp_get_attachment_image_src( $image_id, 'title-image' );
  ?>
  <article <?php post_class(); ?>>
    <header class="page-header" style="background-image: url('<?php echo $image[0]; ?>');">
      <div class="overlay"></div>
      <div class="container">
        <h1 class="entry-title"><?php the_title(); ?></h1>
      </div>
    </header>
    <?php $intro_p = get_field('intro_paragraph'); 
    if ($intro_p != NULL) { ?>
    <div class="page-intro">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
            <?php echo $intro_p ?>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="entry-content container">
    
      <?php 

      $images = get_field('gallery');

      if( $images ): ?>
        <div class="gallery">
          <?php foreach( $images as $image ) { ?>
            <div class="gallery-item">
              <a href="<?php echo $image['sizes']['large']; ?>" class="swipebox">
                <img src="<?php echo $image['sizes']['tile-image']; ?>" alt="<?php echo $image['alt']; ?>" />
              </a>
            </div>
          <?php } ?>
        </ul>
      <?php endif; ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
<?php
/**
 * Template Name: Client Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php $image_id = get_post_meta( get_the_ID(), 'title_background', true );
  $image_array = wp_get_attachment_image_src( $image_id, 'title-image');

  if ($image_id != NULL ) {
    $image = $image_array[0];
  } else {
    $image = get_template_directory_uri() . '/dist/images/title-1.jpg';
  }

  ?>
  <div class="page-header" style="background-image: url('<?php echo $image; ?>');">
    <div class="overlay"></div>
    <div class="container">
      <h1>My Account</h1>
    </div>
  </div>

	<?php $current_user = wp_get_current_user(); ?>
	<div class="submenu">
		<div class="container">
			<div class="welcome">
				<span>Welcome </span><span class="name"><?php echo $current_user->user_firstname ?></span>
			</div>
			<nav>
				<?php
				if (has_nav_menu('client_navigation')) :
				  wp_nav_menu(['theme_location' => 'client_navigation', 'menu_class' => 'nav']);
				endif;
				?>
			</nav>
		</div>
	</div>

  <div class="container">
    <h2 class="page-title"><?php the_field('page_title'); ?></h2>
  </div>

  <?php $intro_p = get_field('intro_paragraph'); 
  if ($intro_p != NULL) { ?>
  <div class="page-intro">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
          <?php echo $intro_p ?>
        </div>
      </div>
    </div>
  </div>
  <?php } ?>
  <div class="page-content">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
          <?php get_template_part('templates/content', 'page'); ?>
        </div>
      </div>
    </div>
  </div>
<?php endwhile; ?>
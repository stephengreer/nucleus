<?php
/**
 * Template Name: Login Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <?php get_template_part('templates/page', 'header'); ?>
  <?php $intro_p = get_field('intro_paragraph'); 
  if ($intro_p != NULL) { ?>
  <div class="page-intro">
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
  				<?php echo $intro_p ?>
  			</div>
  		</div>
  	</div>
	</div>
	<?php } ?>
	<div class="page-content">
	  <div class="container">
	  	<div class="row">
	  		<div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
          <h2 class="page-title">Your Account</h2>
          <p class="login-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et fringilla neque. Phasellus tempor mauris sapien. Curabitur sed orci erat. Mauris ornare odio at quam rhoncus vehicula.</p>
          <div class="user-login">
            <?php echo do_shortcode( '[wpum_login_form login_link="no" psw_link="no" register_link="no"]' ); ?>
          </div>
          <p class="login-details">Don’t have an account? <a href="#">Contact us</a> if you want more information.</p>

				  <?php get_template_part('templates/content', 'page'); ?>
			  </div>
		  </div>
	  </div>
    <div class="contractor-login">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2">
            <h3>Preferred Contractor Login</h3>
            <?php echo do_shortcode( '[wpum_login_form id="2" login_link="no" psw_link="no" register_link="no"]' ); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php endwhile; ?>

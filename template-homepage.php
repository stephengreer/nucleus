<?php
/**
 * Template Name: Homepage Template
 */
?>

<?php 
$image_1 = get_field('hero_background_1');
$image_2 = get_field('hero_background_2');
$image_3 = get_field('hero_background_3');
$image_4 = get_field('hero_background_4');

if ($image_1) {
  $image_url_1 = '<li class="active" style="background-image: url(&quot;' . $image_1['url'] . '&quot;);"></li> ';
} else {
  $image_url_1 = NULL;
}

if ($image_2) {
  $image_url_2 = '<li style="background-image: url(&quot;' . $image_2['url'] . '&quot;);"></li> ';
} else {
  $image_url_2 = NULL;
}

if ($image_3) {
  $image_url_3 = '<li style="background-image: url(&quot;' . $image_3['url'] . '&quot;);"></li> ';
} else {
  $image_url_3 = NULL;
}

if ($image_4) {
  $image_url_4 = '<li style="background-image: url(&quot;' . $image_4['url'] . '&quot;);"></li> ';
} else {
  $image_url_4 = NULL;
}
?>

<section class="hero">
  <div class="hero-bg">
    <ul class="list-unstyled list-inline">
      <?php echo $image_url_1; ?>
      <?php echo $image_url_2; ?>
      <?php echo $image_url_3; ?>
      <?php echo $image_url_4; ?>
    </ul>
  </div>
	<img src="<?php echo get_template_directory_uri() ?>/dist/images/hero-logo.png">
</section>

<?php

// check if the flexible content field has rows of data
if( have_rows('building_blocks') ):
   // loop through the rows of data
  while ( have_rows('building_blocks') ) : the_row();

    if( get_row_layout() == 'cta_right' ):
    	get_template_part( 'templates/section', 'ctaright' );

    elseif( get_row_layout() == 'divider' ): 
    	get_template_part( 'templates/section', 'divider' );

    elseif( get_row_layout() == 'column_slider' ): 
    	get_template_part( 'templates/section', 'columnslider' );

    elseif( get_row_layout() == 'small_gallery' ): 
    	get_template_part( 'templates/section', 'smallgallery' );

    endif;

  endwhile;

else :

    echo "no blocks found";

endif;

?>